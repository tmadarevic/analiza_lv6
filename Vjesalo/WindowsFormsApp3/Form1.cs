﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {
        string file;
        string target;
        int i = 0;
        char guess;
        int tries;
        bool hit = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void randomWordToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 w1 = new Form2();
            if (w1.ShowDialog() == DialogResult.OK)
                file = Form2.path;
            // MessageBox.Show(file);
            StreamReader sr = new StreamReader(file);
            while (sr.ReadLine() != null)
                i++;
            sr.Dispose();
            sr = new StreamReader(file);
            Random r = new Random();
            for (int k = 0; k < r.Next(1, i-1); k++)
                target = sr.ReadLine();
            sr.Dispose();
            MessageBox.Show(target);
            Setting();
        }
        private void Setting()
        {
            label1.Text = "";
            for (i = 0; i < target.Length; i++)
                label1.Text = label1.Text.Insert(i, "*");

        }

        private void Guess_Click(object sender, EventArgs e)
        {
            guess = textLetter.Text[0];
            for (i = 0; i < target.Length; i++)
            {
                if (Char.ToUpper(target[i])==Char.ToUpper(guess))
                {
                    hit = true;
                    label1.Text = label1.Text.Remove(i, 1);
                    label1.Text = label1.Text.Insert(i, guess.ToString());
                }
            }
            if (label1.Text.ToUpper() == target.ToUpper())
                WonGame();

            if (!hit)
            {
                tries++;
                if (tries == 9)
                    LostGame();

            }
            hit = false;
            textLetter.Focus();
            textLetter.SelectAll();
        }
        private void LostGame()
        {
            MessageBox.Show("YOU LOST! Secet word was"+target);
            ResetGame();
        }
        private void WonGame()
        {
            MessageBox.Show("YOU WON!Secet word was"+target);
            ResetGame();
        }
        private void ResetGame()
        {
            textLetter.Text = "";
            label1.Text = "Secret word";
            target = "";
        }
    }
}
