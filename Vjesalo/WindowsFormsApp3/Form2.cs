﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp3
{
    public partial class Form2 : Form
    {
        public static string path;

        public Form2()
        {
            InitializeComponent();
        }

        private void PickAWordList_Click(object sender, EventArgs e)
        {
            OpenFileDialog opd = new OpenFileDialog();
            if (opd.ShowDialog() == DialogResult.OK)
                path = opd.FileName;
            textBox1.Text = path;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void OK_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != null)
            {
                this.Close();
                this.DialogResult = DialogResult.OK;
            }

        }
    }
}
