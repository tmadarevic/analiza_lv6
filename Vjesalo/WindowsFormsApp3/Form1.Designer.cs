﻿namespace WindowsFormsApp3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.SecretWord = new System.Windows.Forms.Label();
            this.GuessLetter = new System.Windows.Forms.Label();
            this.textLetter = new System.Windows.Forms.TextBox();
            this.Guess = new System.Windows.Forms.Button();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.newGameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.randomWordToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(22, 417);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "label1";
            // 
            // SecretWord
            // 
            this.SecretWord.AutoSize = true;
            this.SecretWord.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SecretWord.Location = new System.Drawing.Point(21, 368);
            this.SecretWord.Name = "SecretWord";
            this.SecretWord.Size = new System.Drawing.Size(147, 29);
            this.SecretWord.TabIndex = 1;
            this.SecretWord.Text = "Secret Word";
            this.SecretWord.Click += new System.EventHandler(this.label1_Click);
            // 
            // GuessLetter
            // 
            this.GuessLetter.AutoSize = true;
            this.GuessLetter.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.GuessLetter.Location = new System.Drawing.Point(21, 73);
            this.GuessLetter.Name = "GuessLetter";
            this.GuessLetter.Size = new System.Drawing.Size(142, 29);
            this.GuessLetter.TabIndex = 2;
            this.GuessLetter.Text = "Guess letter";
            // 
            // textLetter
            // 
            this.textLetter.Location = new System.Drawing.Point(221, 81);
            this.textLetter.MaxLength = 1;
            this.textLetter.Name = "textLetter";
            this.textLetter.Size = new System.Drawing.Size(48, 20);
            this.textLetter.TabIndex = 3;
            // 
            // Guess
            // 
            this.Guess.Location = new System.Drawing.Point(26, 120);
            this.Guess.Name = "Guess";
            this.Guess.Size = new System.Drawing.Size(87, 37);
            this.Guess.TabIndex = 4;
            this.Guess.Text = "Guess";
            this.Guess.UseVisualStyleBackColor = true;
            this.Guess.Click += new System.EventHandler(this.Guess_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newGameToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(437, 24);
            this.menuStrip1.TabIndex = 5;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // newGameToolStripMenuItem
            // 
            this.newGameToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.randomWordToolStripMenuItem});
            this.newGameToolStripMenuItem.Name = "newGameToolStripMenuItem";
            this.newGameToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.newGameToolStripMenuItem.Text = "New game";
            // 
            // randomWordToolStripMenuItem
            // 
            this.randomWordToolStripMenuItem.Name = "randomWordToolStripMenuItem";
            this.randomWordToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.randomWordToolStripMenuItem.Text = "Random word";
            this.randomWordToolStripMenuItem.Click += new System.EventHandler(this.randomWordToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(437, 450);
            this.Controls.Add(this.Guess);
            this.Controls.Add(this.textLetter);
            this.Controls.Add(this.GuessLetter);
            this.Controls.Add(this.SecretWord);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label SecretWord;
        private System.Windows.Forms.Label GuessLetter;
        private System.Windows.Forms.TextBox textLetter;
        private System.Windows.Forms.Button Guess;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem newGameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem randomWordToolStripMenuItem;
    }
}

