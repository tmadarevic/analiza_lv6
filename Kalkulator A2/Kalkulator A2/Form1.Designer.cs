﻿namespace Kalkulator_A2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelX = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_x = new System.Windows.Forms.TextBox();
            this.textBox_y = new System.Windows.Forms.TextBox();
            this.sinus = new System.Windows.Forms.Button();
            this.kosinus = new System.Windows.Forms.Button();
            this.log = new System.Windows.Forms.Button();
            this.xx = new System.Windows.Forms.Button();
            this.sqrt = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.AC = new System.Windows.Forms.Button();
            this.rezultat = new System.Windows.Forms.Label();
            this.zbrajanje = new System.Windows.Forms.Button();
            this.oduzimanje = new System.Windows.Forms.Button();
            this.dijeljenje = new System.Windows.Forms.Button();
            this.mnozenje = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelX
            // 
            this.labelX.AutoSize = true;
            this.labelX.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.labelX.Location = new System.Drawing.Point(34, 48);
            this.labelX.Name = "labelX";
            this.labelX.Size = new System.Drawing.Size(20, 20);
            this.labelX.TabIndex = 0;
            this.labelX.Text = "X";
            this.labelX.Click += new System.EventHandler(this.labelX_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(34, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 20);
            this.label1.TabIndex = 1;
            this.label1.Text = "Y";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBox_x
            // 
            this.textBox_x.Location = new System.Drawing.Point(132, 48);
            this.textBox_x.Name = "textBox_x";
            this.textBox_x.Size = new System.Drawing.Size(100, 20);
            this.textBox_x.TabIndex = 2;
            this.textBox_x.TextChanged += new System.EventHandler(this.textBox_x_TextChanged);
            // 
            // textBox_y
            // 
            this.textBox_y.Location = new System.Drawing.Point(132, 92);
            this.textBox_y.Name = "textBox_y";
            this.textBox_y.Size = new System.Drawing.Size(100, 20);
            this.textBox_y.TabIndex = 3;
            this.textBox_y.TextChanged += new System.EventHandler(this.textBox_y_TextChanged);
            // 
            // sinus
            // 
            this.sinus.Location = new System.Drawing.Point(403, 44);
            this.sinus.Name = "sinus";
            this.sinus.Size = new System.Drawing.Size(75, 23);
            this.sinus.TabIndex = 4;
            this.sinus.Text = "sin";
            this.sinus.UseVisualStyleBackColor = true;
            this.sinus.Click += new System.EventHandler(this.sinus_Click);
            // 
            // kosinus
            // 
            this.kosinus.Location = new System.Drawing.Point(598, 48);
            this.kosinus.Name = "kosinus";
            this.kosinus.Size = new System.Drawing.Size(75, 23);
            this.kosinus.TabIndex = 5;
            this.kosinus.Text = "cos";
            this.kosinus.UseVisualStyleBackColor = true;
            this.kosinus.Click += new System.EventHandler(this.kosinus_Click);
            // 
            // log
            // 
            this.log.Location = new System.Drawing.Point(403, 88);
            this.log.Name = "log";
            this.log.Size = new System.Drawing.Size(75, 23);
            this.log.TabIndex = 6;
            this.log.Text = "log";
            this.log.UseVisualStyleBackColor = true;
            this.log.Click += new System.EventHandler(this.log_Click);
            // 
            // xx
            // 
            this.xx.Location = new System.Drawing.Point(598, 88);
            this.xx.Name = "xx";
            this.xx.Size = new System.Drawing.Size(75, 23);
            this.xx.TabIndex = 7;
            this.xx.Text = "x^2";
            this.xx.UseVisualStyleBackColor = true;
            this.xx.Click += new System.EventHandler(this.xx_Click);
            // 
            // sqrt
            // 
            this.sqrt.Location = new System.Drawing.Point(403, 134);
            this.sqrt.Name = "sqrt";
            this.sqrt.Size = new System.Drawing.Size(75, 23);
            this.sqrt.TabIndex = 8;
            this.sqrt.Text = "sqrt";
            this.sqrt.UseVisualStyleBackColor = true;
            this.sqrt.Click += new System.EventHandler(this.sqrt_Click);
            // 
            // quit
            // 
            this.quit.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.quit.Location = new System.Drawing.Point(374, 258);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(118, 67);
            this.quit.TabIndex = 9;
            this.quit.Text = "QUIT";
            this.quit.UseVisualStyleBackColor = true;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // AC
            // 
            this.AC.Location = new System.Drawing.Point(575, 258);
            this.AC.Name = "AC";
            this.AC.Size = new System.Drawing.Size(98, 66);
            this.AC.TabIndex = 10;
            this.AC.Text = "AC";
            this.AC.UseVisualStyleBackColor = true;
            this.AC.Click += new System.EventHandler(this.AC_Click);
            // 
            // rezultat
            // 
            this.rezultat.AutoSize = true;
            this.rezultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.rezultat.Location = new System.Drawing.Point(38, 220);
            this.rezultat.Name = "rezultat";
            this.rezultat.Size = new System.Drawing.Size(73, 20);
            this.rezultat.TabIndex = 11;
            this.rezultat.Text = "Rezultat:";
            this.rezultat.Click += new System.EventHandler(this.rezultat_Click);
            // 
            // zbrajanje
            // 
            this.zbrajanje.Location = new System.Drawing.Point(598, 134);
            this.zbrajanje.Name = "zbrajanje";
            this.zbrajanje.Size = new System.Drawing.Size(75, 23);
            this.zbrajanje.TabIndex = 12;
            this.zbrajanje.Text = "+";
            this.zbrajanje.UseVisualStyleBackColor = true;
            this.zbrajanje.Click += new System.EventHandler(this.zbrajanje_Click);
            // 
            // oduzimanje
            // 
            this.oduzimanje.Location = new System.Drawing.Point(403, 175);
            this.oduzimanje.Name = "oduzimanje";
            this.oduzimanje.Size = new System.Drawing.Size(75, 23);
            this.oduzimanje.TabIndex = 13;
            this.oduzimanje.Text = "-";
            this.oduzimanje.UseVisualStyleBackColor = true;
            this.oduzimanje.Click += new System.EventHandler(this.oduzimanje_Click);
            // 
            // dijeljenje
            // 
            this.dijeljenje.Location = new System.Drawing.Point(598, 174);
            this.dijeljenje.Name = "dijeljenje";
            this.dijeljenje.Size = new System.Drawing.Size(75, 23);
            this.dijeljenje.TabIndex = 14;
            this.dijeljenje.Text = "/";
            this.dijeljenje.UseVisualStyleBackColor = true;
            this.dijeljenje.Click += new System.EventHandler(this.dijeljenje_Click);
            // 
            // mnozenje
            // 
            this.mnozenje.Location = new System.Drawing.Point(403, 220);
            this.mnozenje.Name = "mnozenje";
            this.mnozenje.Size = new System.Drawing.Size(75, 23);
            this.mnozenje.TabIndex = 15;
            this.mnozenje.Text = "*";
            this.mnozenje.UseVisualStyleBackColor = true;
            this.mnozenje.Click += new System.EventHandler(this.mnozenje_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.mnozenje);
            this.Controls.Add(this.dijeljenje);
            this.Controls.Add(this.oduzimanje);
            this.Controls.Add(this.zbrajanje);
            this.Controls.Add(this.rezultat);
            this.Controls.Add(this.AC);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.sqrt);
            this.Controls.Add(this.xx);
            this.Controls.Add(this.log);
            this.Controls.Add(this.kosinus);
            this.Controls.Add(this.sinus);
            this.Controls.Add(this.textBox_y);
            this.Controls.Add(this.textBox_x);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelX);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_x;
        private System.Windows.Forms.TextBox textBox_y;
        private System.Windows.Forms.Button sinus;
        private System.Windows.Forms.Button kosinus;
        private System.Windows.Forms.Button log;
        private System.Windows.Forms.Button xx;
        private System.Windows.Forms.Button sqrt;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Button AC;
        private System.Windows.Forms.Label rezultat;
        private System.Windows.Forms.Button zbrajanje;
        private System.Windows.Forms.Button oduzimanje;
        private System.Windows.Forms.Button dijeljenje;
        private System.Windows.Forms.Button mnozenje;
    }
}

